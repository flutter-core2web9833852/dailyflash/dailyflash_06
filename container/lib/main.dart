import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;
  bool box1 = false;
  bool box2 = false;
  bool box3 = false;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text("Daily Flash"),
        ),
        body: Column(
          children: [
            Image.asset("assets/image.jpg"),
            const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Pizza",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Text("Veg loaded pizza with extra cheese")
                ],
              ),
            )
          ],
        ),
      );
    } else if (count == 2) {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20))),
                child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTY38H61HwZHaCt52x4C7cGsdI4DbQrL8rRXwB6GI-earyZbl0WiyZOU_cD_Vv3HGXnxrw&usqp=CAU",
                  height: 200,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                child: Container(
                  width: 250,
                  height: 70,
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                      child: Text(
                    "Add to cart",
                    style: TextStyle(fontSize: 22),
                  )),
                ),
              )
            ],
          ),
        ),
      );
    } else if (count == 3) {
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.amber,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.greenAccent,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.deepOrangeAccent,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.pinkAccent,
                )
              ],
            )
          ],
        ),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(border: Border.all(width: 1)),
                child: Row(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(border: Border.all(width: 1)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                              color: Colors.amber,
                              border: Border.all(width: 1)),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(border: Border.all(width: 1)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              border: Border.all(width: 1)),
                        ),
                      ),
                    ),
                  )
                ]),
              )
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    box1 = true;
                  });
                },
                child: Container(
                  height: 100,
                  width: 200,
                  decoration: BoxDecoration(
                      color: (box1) ? Colors.red : Colors.white,
                      border: Border.all(width: 1)),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    box2 = true;
                  });
                },
                child: Container(
                  height: 100,
                  width: 200,
                  decoration: BoxDecoration(
                      color: (box2)
                          ? Color.fromARGB(255, 0, 255, 34)
                          : Colors.white,
                      border: Border.all(width: 1)),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    box3 = true;
                  });
                },
                child: Container(
                  height: 100,
                  width: 200,
                  decoration: BoxDecoration(
                      color: (box3)
                          ? Color.fromARGB(255, 0, 72, 255)
                          : Colors.white,
                      border: Border.all(width: 1)),
                ),
              )
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
